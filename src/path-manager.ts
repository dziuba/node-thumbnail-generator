import Store from 'data-store';
import networkDrive from 'windows-network-drive';

export default class PathManager {
    private imageDrive: string;
    private imagePath: string;
    private imageIsNetwork: boolean;

    private thumbsDrive: string;
    private thumbsPath: string;
    private thumbsIsNetwork: boolean;

    private config: any;

    private userName: string;
    private password: string;

    constructor(){
        this.imageDrive = "";
        this.imagePath = "";
        this.imageIsNetwork = false;

        this.thumbsDrive = "";
        this.thumbsPath = "";
        this.thumbsIsNetwork = false;

        this.config = new Store({ path: 'config.json' });



        if(this.config.has("imageDir")){
            this.imagePath = this.config.get("imageDir");
        }else{
            throw "Check config file!";
        }

        if(this.config.has("imageNetworkDrive")){
            this.imageIsNetwork = this.config.get("imageNetworkDrive");
        }else{         
            throw "Check config file!";
        }

        if(this.config.has("imageNetworkLetter")){
            this.imageDrive =  this.config.get("imageNetworkLetter");
        }else{
            throw "Check config file!";
        }

        if(this.config.has("thumbsDir")){
            this.thumbsPath = this.config.get("thumbsDir");
        }else{
            throw "Check config file!";
        }

        if(this.config.has("thumbsNetworkDrive")){
            this.thumbsIsNetwork = this.config.get("thumbsNetworkDrive");
        }else{
            throw "Check config file!";
        }

        if(this.config.has("thumbsNetworkLetter")){
            this.thumbsDrive =  this.config.get("thumbsNetworkLetter");
        }else{
            throw "Check config file!";
        }

        if(this.config.has("networkUserName")){
            this.userName =  this.config.get("networkUserName");
        }else{
            throw "Check config file!";
        }

        if(this.config.has("networkPassword")){
            this.password =  this.config.get("networkPassword");
        }else{
            throw "Check config file!";
        }
    }

    public GetImageDir(): string {
        if(this.imageIsNetwork){
            return this.imageDrive + ":\\";
        }else{
            return this.imagePath;
        }
    }

    public GetThumbsDir(): string {
        if(this.thumbsIsNetwork){
            return this.thumbsDrive + ":\\";
        }else{
            return this.thumbsPath;
        }
    }

    public OpenPaths(): Promise<void> {
        return new Promise( (resolve, reject) => {
            let promises: Array<Promise<void>> = [];

            promises.push(this.OpenPath(this.imagePath, this.imageDrive, this.imageIsNetwork));
            promises.push(this.OpenPath(this.thumbsPath, this.thumbsDrive, this.thumbsIsNetwork));

            Promise.all(promises).then( () => {
                resolve();
            }).catch( err => {
                reject(err);
            })

        })
    }

    public ClosePaths(): Promise<void> {
        return new Promise( (resolve, reject) => {
            let promises: Array<Promise<void>> = [];

            promises.push(networkDrive.unmount(this.imageDrive));
            promises.push(networkDrive.unmount(this.thumbsDrive));

            Promise.all(promises).then( () => {
                resolve();
            }).catch( () => {
                reject();
            })


        });
    }

    private OpenPath(netPath: string, netLetter: string, netIsIt: boolean): Promise<void> {
        return new Promise( (resolve, reject) => {

            if(netIsIt === false){
                resolve();
            }else{
                networkDrive.find(netPath)
                .then( (drives: Array<string>) => {
                    if(drives.length === 0){
                        networkDrive.mount(netPath, netLetter, this.userName, this.password)
                        .then( (letter: string) => 
                        {
                            resolve();
                        })
                        .catch( (err: string) => {
                            reject(err);
                        });
                    }else{
                        resolve();
                    }
                }).
                catch( (err: string) => {
                    reject(err);
                });
            }
        });
    }
}