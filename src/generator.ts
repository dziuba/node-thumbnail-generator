import fs from 'fs';
import path from 'path';
import glob from 'glob';
import sharp from 'sharp'
import Store from 'data-store';
import {GetDirs, GetThumbnailsDir, RemoveDirsIfEmpty} from './helpers';
import PathManager from './path-manager';

const config = new Store({ path: 'config.json' });

export function Generate(generateAll?: boolean, resolution?: Array<number>): Promise<void> {
    return new Promise( (resolve, reject) => {
        let pathManager = new PathManager();

        pathManager.OpenPaths().then(() =>{
            GenerateWorker(pathManager.GetImageDir(), pathManager.GetThumbsDir(), generateAll, resolution).then( () =>{
                pathManager.ClosePaths().then( () => {
                    resolve();
                }).catch( err => {
                    console.error(err);
                    resolve();
                });
            }).catch( err => {
                pathManager.ClosePaths().then( () => {
                    console.error(err);
                    reject(err);
                }).catch( () => {
                    console.error(err);
                    reject(err);
                });          
            })
        }).catch( err => {
            console.error(err);
            reject(err);
        })
    });
}

function GenerateWorker(imageDir: string, thumbsDir: string, generateAll?: boolean, resolution?: Array<number>): Promise<void> {
    return new Promise(resolve => {
        // Gets last generate time
        if(generateAll === undefined || generateAll === false){
            generateAll = false;

            if(config.has("generateAll")){
                generateAll = config.get("generateAll");
            }
        }       

        var generateTime = new Date(2000, 1, 1);
        var newGenerateTime = new Date();
        if(!generateAll)
            if(config.has("lastGenerateTime")){
                generateTime = new Date(Date.parse(config.get("lastGenerateTime")));
            }

        // Get thumbnail resolutions
        var res: Array<number> = [50];
        if(resolution === undefined || resolution[0] === 0){
            if(config.has("thumbsSizes")){
                res = config.get("thumbsSizes");
            }            
        }else{
            res = resolution;
        }

        // Get thumbnail prefix
        var prefix: string = "thumbs";
        if(config.has("thumbsDirPrefix")){
            prefix = config.get("thumbsDirPrefix");
        } 
        
        // Create ingoreDirs
        let ignoreDirs: Array<string> = [];

        // Create thumbs direcotries
        for(let i = 0; i < res.length ; i++){
            let d = GetThumbnailsDir(prefix, res[i], thumbsDir);
            ignoreDirs.push(d);
            if(!fs.existsSync(d)){
                fs.mkdirSync(d, {recursive: true});
            }
        }

        console.log("Przygotowuję listę katalogów");

        let dirs: Array<string> = [];
        try{
            dirs = GetDirs(imageDir, ignoreDirs);
        }catch(e){
            console.error(e);
            process.exit(1);
        }
        
        console.log("Rozpoczynam generowanie miniaturek");

        let promises: Array<Promise<void>> = [];
        dirs.forEach((dir: string) => {
            promises.push(new Promise(resolve => {
                glob(dir +"\\*.{jpg,jpeg,gif,tif,png}", function (er, files) {

                    let promises: Array<Promise<void>> = [];
            
                    files.forEach(file => {
                        let promise: Promise<void> = new Promise((resolve, reject) => {
                            let name = path.basename(file);
                            fs.stat(file, (err, stats) => {
                                if(stats !== undefined && stats.ctime < generateTime){
                                    reject();
                                }else{
                                    let buffer = fs.readFileSync(file);

                                    let promises: Array<Promise<void>> = [];

                                    res.forEach(r => {
                                        promises.push(new Promise(resolve => {
                                            let thumbFile = GetThumbnailsDir(prefix, r, thumbsDir, true) + name;
                    
                                           sharp(buffer)
                                                .resize(r)
                                                .toFile(thumbFile, (err, info) => { 
                                                    resolve();
                                                });
                                        }));
                                    });

                                    Promise.all(promises).then(() => {
                                        console.log("Wygenerowano: "+ name);
                                        config.set("lastGenerateTime", newGenerateTime);
                                        resolve();
                                    });
                                }   
                            });
                        });
            
                        promises.push(promise);
                         
                    });
            
                    Promise.all(promises)
                    .then(() => {
                        resolve();
                    })
                    .catch(() => {
                        resolve();
                    });
                });
            }));
        });

        Promise.all(promises).then( () => {
            console.log("Zakończono generowanie");
            //resolve();
        })
        .catch(() => {
            console.log("Zakończono generowanie");
            //resolve();
        })
        .then(() => {
            //Removes empty thumbnails directories
            console.log("Usuwam puste katalogi");
            RemoveDirsIfEmpty(ignoreDirs);
            resolve()
        });
    });
    

    
}
