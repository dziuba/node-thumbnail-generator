import schedule from 'node-schedule';
import Store from 'data-store';
import {Generate} from './generator';

const config = new Store({ path: 'config.json' });

var crons: Array<string> = [];
if(config.has("cron")){
    crons = config.get("cron");
}else{
    console.log("Brak ustawień cron");
    process.exit(1);
}

crons.forEach(cron => {
    schedule.scheduleJob(cron, () => {
        Generate();
    });
});


