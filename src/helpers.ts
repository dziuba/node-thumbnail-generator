import fs from 'fs';

export function GetDirs(path: string, ignorePaths: Array<string>): Array<string> {

    if(ignorePaths !== undefined && ignorePaths.includes(path)){
        throw "Ignoruję katalog";
    }

    let dirs: string[] = [];

    dirs.push(path);

    let elements = fs.readdirSync(path);

    for(let i = 0; i < elements.length ; i++){
        let el = elements[i];
        let p = path + "\\" + el;

        let info =  fs.statSync(p);

        if(info.isDirectory()){
            try{
                dirs = dirs.concat(GetDirs(p, ignorePaths));
            }catch(e){

            }
        }
    }

    return  dirs;

}

export function GetThumbnailsDir(prefix: string, resolution: number, path?: string, slash?: boolean): string {
    let thumbDir:string = prefix + resolution;
    let finish: boolean = false;
    if(slash !== undefined){
        finish = slash;
    }

    if(path === undefined){
        return thumbDir;
    }else{
        return path + "\\" + thumbDir + (finish ? "\\" : "");
    }
}

export function RemoveDirsIfEmpty(dirs: Array<string>): void {
    for(let i = 0 ; i < dirs.length ; i++){
        let files = fs.readdirSync(dirs[i]);
        if(!files.length){
            try{
                fs.rmdirSync(dirs[i]);
            }catch(e){

            }
        }
    }
}