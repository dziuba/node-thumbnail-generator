import schedule from 'node-schedule';
import {argv} from 'yargs';
import Store from 'data-store';
import {Generate} from './generator';


// DEBUG
// @ts-ignore
var debug = typeof v8debug === 'object' 
            || /--debug|--inspect/.test(process.execArgv.join(' '));

if(debug){
    Generate().then(() => {
        console.log("Koniec");
        process.exit(0);
    });
}else{
    if(argv.run == "service"){
        const config = new Store({ path: 'config.json' });
    
        var crons: string[] = [];
        if(config.has("cron")){
            crons = config.get("cron");
        }else{
            console.log("Brak ustawień cron");
            process.exit(1);
        }
    
        crons.forEach(cron => {
            schedule.scheduleJob(cron, () => {
                Generate();
            });
        });
    }else if(argv.run == "app"){
        let res: number = 0;
        if(argv.resolution !== undefined){
            // @ts-ignore
            res = parseInt(argv.resolution);
        }

        let all:  boolean = false;
        if(argv.all !== undefined){
            all = argv.all ? true : false;
        }

        Generate(all, [res]).then(() => {
            console.log("Koniec");
            process.exit(0);
        });
    }else{
        console.log("Parametry:");
        console.log("--run=service: Uruchomo jako serwis");
        console.log("--run=app: Uruchomo jako aplikacja");
        console.log("\t--resolution=500: Wygeneruj rozmiar 500px");
        console.log("\t--all=true: Generuj miniaturki dla wszystkich obrazów");
    }
}


