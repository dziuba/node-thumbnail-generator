import {argv} from 'yargs';
import Store from 'data-store';
import {Service} from 'node-windows';

const config = new Store({ path: 'config.json' });

var serviceScript = "";
    if(config.has("serviceScript")){
        serviceScript = config.get("serviceScript");
    }else{
        console.log("Brak skryptu service.js");
        process.exit(1);
    }

// Get service info
let sName: string = "Node Thumbnail Generator";
let sDescription: string = "Generates thumbnails for rockerspro catalog.";

if(config.has("serviceName")){
    sName = config.get("serviceName");
}

if(config.has("serviceDescription")){
    sDescription = config.get("serviceDescription");
}

// Create a new service object
var svc = new Service({
  name: sName,
  description: sDescription,
  script: serviceScript,
  nodeOptions: [
    '--harmony',
    '--max_old_space_size=4096'
  ]
});

if(argv.action == "install"){
    // Listen for the "install" event, which indicates the
    // process is available as a service.
    svc.on('install',function(){
        svc.start();
    });
    
    svc.install();
}else if(argv.action == "uninstall"){
    // Listen for the "uninstall" event so we know when it's done.
    svc.on('uninstall',function(){
        console.log('Uninstall complete.');
        console.log('The service exists: ',svc.exists);
    });
    
    // Uninstall the service.
    svc.uninstall();
}else{
    console.log("Parametry:");
    console.log("--action=install: Instaluje serwis");
    console.log("--action=uninstall: Usuwa serwis");
}

