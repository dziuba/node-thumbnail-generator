# Node Thumbnail Generator

## Aplikacja - uruchamianie
1. app.js - można uruchomić w trybie cron oraz pojedynczego generowania
2. service.js - uruchamiany w trybie cron

## Aplikacja / Serwis - konfiguracja
Config.json musi znajdować się w katalogu build lub innym produkcjnym.

## Serwis - instalacja
1. Zainstaluj moduł globalnie -> npm install -g node-windows
2. Podlinkuj moduł -> npm link node-windows
3. W config.json ustaw ścieżkę do skryptu service.js
4. Uruchom service-install.js 